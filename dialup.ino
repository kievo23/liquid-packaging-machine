#include <Keypad.h>

const byte ROWS = 4; 
const byte COLS = 4; 

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {9, 8, 7, 6}; 
byte colPins[COLS] = {5, 4, 3}; 

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

//Water flow sensor
int flowPin = 2;    //This is the input pin on the Arduino
double flowRate;    //This is the value we intend to calculate. 
double liquidFlowPerSession = 0;  //Will record the amount of liquid that has passed through cummulatively
volatile int flow_frequency; //This integer needs to be set as volatile to ensure it updates correctly during the interrupt process.
boolean relayStatus = false; //relay is on 
int relayTimerCount = 0;
int setVolume = 0; //sets the volume to refill per session 
unsigned long currentTime;
unsigned long previousTime;
byte sensorInterrupt;
String charValue;
char buf[3];

int waterPumpRelayPin = 11;    //This is the input pin on the Arduino

void setup(){
  // put your setup code here, to run once:
  
  //waterflow
  pinMode(flowPin, INPUT);           //Sets the pin as an input
  Serial.begin(9600);  //Start Serial
  attachInterrupt(sensorInterrupt, Flow, RISING);  //Configures interrupt 0 (pin 2 on the Arduino Uno) to run the function "Flow"  

  //Water Pump Relay Pin
  pinMode(waterPumpRelayPin, OUTPUT);
  digitalWrite(waterPumpRelayPin,LOW);

  interrupts();   //Enables interrupts on the Arduino
  currentTime = millis();
  previousTime = currentTime;
}

void loop() {
  // put your main code here, to run repeatedly:
  if(relayStatus == true){
    waterflow();
  }

  //Keypad Function
  char customKey = customKeypad.getKey();
  
  if (customKey){
    if(charValue.length() >= 0 && charValue.length() < 3){ //check if volume to be set is atleast two digit or 3 digits
      charValue.concat(customKey);
    }
    if(customKey == '*' && digitalRead(waterPumpRelayPin) == LOW){ //stops the pump
        relayStatus = false;
        setVolume = 0;
        charValue = "";
        relayTimerCount = 0;
    }

    if(customKey == '#' ){ //starts the pump
      if(charValue.toInt() >= 30 && charValue.toInt() <= 200){
        setVolume = charValue.toInt();
        relayStatus = true;
        digitalWrite(waterPumpRelayPin,HIGH);
      }else{
        Serial.println("Value should be between 30 to 200");
      }
    }
    Serial.println(charValue);
  }   
}

void waterflow(){
  currentTime = millis();
  if(currentTime >= (previousTime + 1000)){
    //Disable the interrupt while calculating the flow rate and sending the value to the host
    detachInterrupt(sensorInterrupt);
    previousTime = currentTime;
    
    //Start the math
    flowRate = (flow_frequency * 0.17);        //Take counted pulses in the last second and multiply by 2.25mL 
    liquidFlowPerSession += flowRate;  // calculate the amount of milli liters passed
    Serial.println(" Flow Rate: " + String(flow_frequency));
    flowRate = flowRate * 60;         //Convert seconds to minutes, giving you mL / Minute
    flowRate = flowRate / 1000;       //Convert mL to Liters, giving you Liters / Minute
   
    Serial.println("Count: " + String(flow_frequency)+" Flow Rate: " + String(flowRate) + " Milliliters passed"+ liquidFlowPerSession + " Relay Timer Count: "+relayTimerCount + ", Relay Status"+ relayStatus); 
    if(liquidFlowPerSession > setVolume){
      digitalWrite(waterPumpRelayPin,LOW);
      //delay(4000);
      liquidFlowPerSession = 0;
      relayTimerCount = 0;
    }
  
    // code to switch on the relay
    if(digitalRead(waterPumpRelayPin) == LOW){
      relayTimerCount++;  //increment by one
      if(relayTimerCount == 15){
         digitalWrite(waterPumpRelayPin,HIGH); // switch on the relay
         relayTimerCount = 0; //reset timer to zero
         relayStatus = true;
      }
    }
    flow_frequency = 0;     // Reset the counter so we start counting from 0 again
    //Enable the interrupt again
    attachInterrupt(sensorInterrupt, Flow, RISING);  //Configures interrupt 0 (pin 2 on the Arduino Uno) to run the function "Flow"  
  }
}

void Flow(){
   flow_frequency++; //Every time this function is called, increment "count" by 1
}
